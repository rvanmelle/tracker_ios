//
//  Timers.swift
//  Tracker
//
//  Created by Reid van Melle on 2016-10-21.
//  Copyright © 2016 Reid van Melle. All rights reserved.
//

import Foundation

private class DebounceHandler: NSObject {

    let action: () -> Void

    init(_ action: @escaping () -> Void) {
        self.action = action
    }

    @objc func handle() {
        action()
    }

}

// Creates and returns a new debounced version of the passed function
// which will postpone its execution until after delay seconds have elapsed
// since the last time it was invoked.
public func debounce(_ delay: TimeInterval, action: @escaping () -> Void) -> () -> Void {
    let handler = DebounceHandler(action)
    var timer: Timer?
    return {
        if let timer = timer {
            timer.invalidate() // if calling again, invalidate the last timer
        }
        timer = Timer(timeInterval: delay, target: handler, selector: #selector(DebounceHandler.handle), userInfo: nil, repeats: false)
        RunLoop.current.add(timer!, forMode: RunLoopMode.defaultRunLoopMode)
    }
}
