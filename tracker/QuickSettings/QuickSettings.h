//
//  QuickSettings.h
//  QuickSettings
//
//  Created by Reid van Melle on 2017-04-01.
//  Copyright © 2017 Reid van Melle Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for QuickSettings.
FOUNDATION_EXPORT double QuickSettingsVersionNumber;

//! Project version string for QuickSettings.
FOUNDATION_EXPORT const unsigned char QuickSettingsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <QuickSettings/PublicHeader.h>


