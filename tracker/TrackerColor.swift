//
//  TrackerColor.swift
//  Tracker
//
//  Created by Reid van Melle on 2016-11-03.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

import Foundation
import Chameleon

//private let colorArray = ColorSchemeOf(ColorScheme.analogous, color: UIColor.flatPlum(), isFlatScheme: true)

private let colorArray = ColorSchemeOf(ColorScheme.complementary, color: UIColor.flatPlum(), isFlatScheme: true)

struct ETColor {
    static var bg1 = colorArray[2]
    static var fg1 = UIColor(complementaryFlatColorOf: bg1)

    static var bg2 = colorArray[1]
    static var fg2 = UIColor(complementaryFlatColorOf: bg2)

    static var bg3 = colorArray[0]
    static var fg3 = UIColor(complementaryFlatColorOf: bg3)

    //static var background = colorArray[4]
}
