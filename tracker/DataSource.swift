protocol DataSourceDelegate: class {
    associatedtype Object
    func cellIdentifierForObject(object: Object) -> String
    func titleForHeaderInSection(section: Int) -> String?
}
