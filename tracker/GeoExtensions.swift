//
//  GeoExtensions.swift
//  Tracker
//
//  Created by Reid van Melle on 2016-11-03.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit
import TrackerModel

extension WayPoint {}

extension CLLocation {

    func computePlacemark(_ completion:@escaping (CLPlacemark) -> Void) {
        CLGeocoder().reverseGeocodeLocation(self) { (placemarks, _) in
            if let placemarks = placemarks {
                let placemark = placemarks[0]
                completion(placemark)
            }
        }
    }

    func computeMapItem(_ completion:@escaping (MKMapItem) -> Void) {
        self.computePlacemark { placemark in
            completion(MKMapItem(placemark: placemark.mapPlacemark))
        }
    }

}

extension CLPlacemark {
    // ABCreateStringWithAddressDictionary ? AddressBookUI
    var formattedAddress: String {
        guard let components = addressDictionary?["FormattedAddressLines"] as? [String] else { return "" }
        return components.joined(separator: ", ")
    }

    var mapPlacemark: MKPlacemark {
        return MKPlacemark(coordinate: location!.coordinate,
                           addressDictionary: addressDictionary as? [String:Any])
    }
}
