import Foundation
import CoreData

public final class ManagedObjectObserver {
    public enum ChangeType {
        case delete
        case update
    }

    public init?(object: ManagedObjectType, changeHandler: @escaping (ChangeType) -> Void) {
        guard let moc = object.managedObjectContext else { return nil }
        objectHasBeenDeleted = false
        token = moc.addObjectsDidChangeNotificationObserver { [unowned self] note in
            guard let changeType = self.changeTypeOfObject(object, inNotification: note) else { return }
            self.objectHasBeenDeleted = changeType == .delete
            changeHandler(changeType)
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(token)
    }

    // MARK: Private

    fileprivate var token: NSObjectProtocol!
    fileprivate var objectHasBeenDeleted: Bool = false

    fileprivate func changeTypeOfObject(_ object: ManagedObjectType,
                                        inNotification note: ObjectsDidChangeNotification) -> ChangeType? {
        let deleted = note.deletedObjects.union(note.invalidatedObjects)
        if note.invalidatedAllObjects || deleted.containsObjectIdenticalTo(object) {
            return .delete
        }
        let updated = note.updatedObjects.union(note.refreshedObjects)
        if updated.containsObjectIdenticalTo(object) {
            return .update
        }
        return nil
    }
}
