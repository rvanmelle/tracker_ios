import CoreData

extension Collection where Iterator.Element: NSManagedObject {
    public func fetchObjectsThatAreFaults() {
        guard !self.isEmpty else { return }
        guard let context = self.first?.managedObjectContext else { fatalError("Managed object must have context") }
        let faults = self.filter { $0.isFault }
        guard let mo = faults.first else { return }
        let request = NSFetchRequest<NSFetchRequestResult>()
        request.entity = mo.entity
        request.returnsObjectsAsFaults = false
        request.predicate = NSPredicate(format: "self in %@", faults)
        do { try context.fetch(request) } catch { fatalError() }
    }
}
