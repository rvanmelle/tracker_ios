//
//  CoreDataHelpers.h
//  CoreDataHelpers
//
//  Created by Reid van Melle on 2016-10-18.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CoreDataHelpers.
FOUNDATION_EXPORT double CoreDataHelpersVersionNumber;

//! Project version string for CoreDataHelpers.
FOUNDATION_EXPORT const unsigned char CoreDataHelpersVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoreDataHelpers/PublicHeader.h>


