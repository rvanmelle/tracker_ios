//
//  LocationRequestor.swift
//  Tracker
//
//  Created by Reid van Melle on 2016-10-28.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

protocol LocationRequestorDelegate: class {
    func locationRequestorDidGetLocation(location: CLLocation)
}

class LocationRequestor: NSObject {

    var authorized: Bool {
        let status = CLLocationManager.authorizationStatus()
        return status == .authorizedAlways || status == .authorizedWhenInUse
    }

    func getQuickLocationUpdate() {
        // Request location authorization
        locationManager.requestWhenInUseAuthorization()

        // Request a location update
        locationManager.requestLocation()
    }

    required init(delegate: LocationRequestorDelegate) {
        super.init()
        self.delegate = delegate
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
    }

    // MARK: Private
    fileprivate weak var delegate: LocationRequestorDelegate!
    private var locationManager: CLLocationManager = CLLocationManager()

}

extension LocationRequestor: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        delegate.locationRequestorDidGetLocation(location: locations.last!)
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }

}
