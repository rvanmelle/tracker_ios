//
//  TrackerViewController.swift
//  Tracker
//
//  Created by Reid van Melle on 2016-10-20.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

import UIKit
import CoreData

class TrackerViewController: UIViewController, ManagedObjectContextSettable {

    var managedObjectContext: NSManagedObjectContext!

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.hidesNavigationBarHairline = true
    }

}
