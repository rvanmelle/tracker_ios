//
//  ActivityTypeController.swift
//  Tracker
//
//  Created by Reid van Melle on 2016-10-22.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

import Foundation
import CoreMotion
import TrackerModel

protocol ActivityTypeControllerDelegate: class {
    func activityTrackderDidChangeActivity(activity: ActivityType)
}

class ActivityTypeController: NSObject {

    fileprivate weak var delegate: ActivityTypeControllerDelegate!
    private var motionManager: CMMotionActivityManager!
    private var queue: OperationQueue!

    static let isAvailable = CMMotionActivityManager.isActivityAvailable()

    public var currentActivityType: ActivityType = .unknown {
        didSet {
            if currentActivityType != oldValue {
                delegate?.activityTrackderDidChangeActivity(activity: currentActivityType)
            }
        }
    }

    required init(delegate: ActivityTypeControllerDelegate? = nil) {
        super.init()
        self.delegate = delegate
        if ActivityTypeController.isAvailable {
            motionManager = CMMotionActivityManager()
            queue = OperationQueue()
            motionManager.startActivityUpdates(to: queue, withHandler: { (activity) in
                if let activity = activity {
                    if activity.automotive {
                        self.currentActivityType = .automotive
                    } else if activity.cycling {
                        self.currentActivityType = .cycling
                    } else if activity.running {
                        self.currentActivityType = .running
                    } else if activity.walking {
                        self.currentActivityType = .walking
                    } else if activity.stationary {
                        self.currentActivityType = .stationary
                    } else {
                        self.currentActivityType = .unknown
                    }
                }
            })
        }
    }
}
