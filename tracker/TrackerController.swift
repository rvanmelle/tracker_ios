//
//  TrackerController.swift
//  Tracker
//
//  Created by Reid van Melle on 2016-10-21.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import MapKit
import TrackerModel
import AudioToolbox

class TrackerController: NSObject, ManagedObjectContextSettable {

    var managedObjectContext: NSManagedObjectContext!
    var tracker: GeoLocationController!
    var motionTracker: ActivityTypeController?
    fileprivate var lastWayPoint: WayPoint?
    fileprivate let createNewPointThreshold  = CLLocationDistance(100)

    var home: MKMapItem?
    fileprivate var currentRegionUID: String?

    fileprivate var trackingMode: GeoLocationController.TrackingMode {
        switch Settings.trackingMode {
        case .DistanceFilter:
            return GeoLocationController.TrackingMode.distanceFilter(Settings.trackingAccuracy.updateDistance)
        case .Fitness:
            return GeoLocationController.TrackingMode.fitness(Settings.trackingAccuracy.locationAccuracy)
        case .SignificantLocationChange:
            return GeoLocationController.TrackingMode.significantChange
        case .Visits:
            return GeoLocationController.TrackingMode.visits
        }
    }

    required init(moc: NSManagedObjectContext, referenceLocation: NamedLocation?) {
        super.init()

        managedObjectContext = moc

        // Setup tracker
        tracker = GeoLocationController(mode:trackingMode, delegate: self)
        let allLocations = NamedLocation.fetchInContext(managedObjectContext)
        let regions = allLocations.map { (loc) -> CLCircularRegion in
            return CLCircularRegion(center: loc.location!.coordinate, radius: CLLocationDistance(loc.radius), identifier: loc.uid!)
        }
        tracker.regionsToMonitor = regions

        // Setup motion tracker
        if ActivityTypeController.isAvailable {
            motionTracker = ActivityTypeController(delegate:nil)
        }

        referenceLocation?.location?.computeMapItem({ (mapItem) in
            self.home = mapItem
        })
    }
}

extension TrackerController : GeoLocationControllerDelegate {

    func geoLocationDidEnterRegion(region: CLRegion) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        currentRegionUID = region.identifier
    }

    func geoLocationDidExitRegion(region: CLRegion) {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        currentRegionUID = nil
    }

    func geoLocationDidChangeLocation(location: CLLocation) {
        let moc = managedObjectContext!
        if !tracker.trackingMode.isDistanceFilter, let lastWayPoint = lastWayPoint, lastWayPoint.location != nil {
            guard location.distance(from: lastWayPoint.location!) > createNewPointThreshold else {
                lastWayPoint.hits += 1
                lastWayPoint.lastTrackedAt = Date()
                return
            }
        }

        CLGeocoder().reverseGeocodeLocation(location) { (placemarks, _) in
            if let placemarks = placemarks {
                let placemark = placemarks[0]
                let point: WayPoint = moc.insertObject()
                point.location = location
                point.createdAt = Date()
                point.address = placemark.formattedAddress
                point.hits = 0
                point.trackingMode = self.tracker.trackingMode.description
                point.pointOfInterest = placemark.areasOfInterest?.first ?? placemark.name
                point.activityType = self.motionTracker?.currentActivityType
                point.locationUID = self.currentRegionUID
                self.lastWayPoint = point
                let _ = point.normalizedDate
                //point.pointOfInterest =

                if let home = self.home {
                    let request: MKDirectionsRequest = MKDirectionsRequest()
                    request.source = MKMapItem(placemark: placemark.mapPlacemark)
                    request.destination = home
                    request.requestsAlternateRoutes = true
                    // todo("replace transport type with known OR default to automobile")
                    request.transportType = .automobile
                    let directions = MKDirections(request: request)
                    directions.calculateETA { (etaResponse, _) in
                        if let eta = etaResponse {
                            point.distance = Float(eta.expectedTravelTime)
                            TrackerNotification.post(.saveRequested)
                            //print("travel time: \(eta.expectedTravelTime)")
                        }
                    }
                } else {
                    // Not really a fallback at this point
                }
            }
        }
    }

    func geoLocationDidChangeAuthorizationStatus(authorized: Bool) {
    }

}
