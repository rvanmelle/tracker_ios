//
//  ModelController.swift
//  pager
//
//  Created by Reid van Melle on 2016-11-02.
//  Copyright © 2016 Reid van Melle. All rights reserved.
//

import UIKit
import CoreData
import TrackerKit
import TrackerModel
import Timepiece

class ActivityModelController: NSObject, UIPageViewControllerDataSource, ManagedObjectContextSettable {

    var pageData: [String] = []

    var managedObjectContext: NSManagedObjectContext!

    init(_ moc: NSManagedObjectContext) {
        managedObjectContext = moc
        super.init()

        // Create the data model.
        let dateFormatter = DateFormatter()
        pageData = dateFormatter.monthSymbols
    }

    private func mapViewController(for date: Date) -> WayPointMapViewController {
        guard let vc = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: "WayPointMapViewController") as? WayPointMapViewController else {
                fatalError()
        }
        vc.date = date
        vc.managedObjectContext = managedObjectContext
        let points = WayPoint.wayPoints(onDate: date, from: managedObjectContext)//.map { return $0.location!  }
        vc.points = points
        vc.date = date
        return vc
    }

    func viewControllerForToday() -> WayPointMapViewController {
        return mapViewController(for: Date.today())
    }

    func dateFromViewController(_ viewController: WayPointMapViewController) -> Date {
        return viewController.date!
    }

    // MARK: - Page View Controller Data Source

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewController = viewController as? WayPointMapViewController else { return nil }
        let date = dateFromViewController(viewController)

        // search for the next point which is less than today
        guard let nextPreviousPoint = WayPoint.firstWayPoint(previousToDate: date, from: managedObjectContext) else {
            return nil
        }

        let previousDate = nextPreviousPoint.createdAt!
        return mapViewController(for: previousDate)
    }

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewController = viewController as? WayPointMapViewController else { return nil }
        let date = dateFromViewController(viewController)

        // search for the next point which is greater than today
        guard let nextPoint = WayPoint.nextWayPoint(afterDate: date, from: managedObjectContext) else {
            return nil
        }
        let nextDate = nextPoint.createdAt!
        return mapViewController(for: nextDate)
    }

}
