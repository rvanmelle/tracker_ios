//
//  WayPointsViewController.swift
//  Tracker
//
//  Created by Reid van Melle on 2016-10-19.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

import UIKit
import TrackerModel
import CoreData
import CoreDataHelpers
import CoreLocation
import MapKit
import Timepiece
import FontAwesomeKit

extension UITabBarItem {
    var icon: FAKIcon {
        get {
            fatalError()
        }
        set {
            newValue.iconFontSize = 30.0
            newValue.addAttribute(NSForegroundColorAttributeName, value: UIColor.black)
            image = newValue.image(with: CGSize(width: 30.0, height: 30.0))
        }
    }
}

class WayPointsViewController: TrackerViewController, SegueHandlerType {

    enum SegueIdentifier: String {
        case ShowWayPoint = "MapWayPointSegue"
    }

    @IBOutlet weak var tableView: UITableView!

    override func awakeFromNib() {
        super.awakeFromNib()
        tabBarItem.icon = FAKMaterialIcons.homeIcon(withSize: 30.0)
        tabBarItem.title = "History"
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tabBarItem.icon = FAKFontAwesome.historyIcon(withSize: 30.0)
        navigationItem.title = "All WayPoints"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Clear", style: .plain, target: self, action: #selector(clearAllData))
        setupTableView()
    }

    func clearAllData() {
        let fetchRequest = WayPoint.fetchRequest()
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try managedObjectContext.persistentStoreCoordinator?.execute(deleteRequest, with: managedObjectContext)
            //managedObjectContext.refreshAllObjects()
            dataProvider.forceRefresh()
            tableView.reloadData()
        } catch _ as NSError {
            fatalError()
        }
    }
    // MARK: Private

    fileprivate typealias Data = FetchedResultsDataProvider<WayPointsViewController>
    fileprivate var dataSource: TableViewDataSource<WayPointsViewController, Data, WayPointTableViewCell>!
    fileprivate var dataProvider: Data!
    private var observer: ManagedObjectObserver?

    private func setupTableView() {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        tableView.delegate = self
        guard let request = WayPoint.sortedFetchRequest as? NSFetchRequest<WayPoint> else { fatalError() }
        request.sortDescriptors = [NSSortDescriptor(key: "normalizedDate", ascending: false),
                                   NSSortDescriptor(key: "createdAt", ascending: false)]
        request.returnsObjectsAsFaults = false
        request.fetchBatchSize = 20
        let frc = NSFetchedResultsController<WayPoint>(fetchRequest: request, managedObjectContext: managedObjectContext,
                                                       sectionNameKeyPath: "normalizedDate", cacheName: nil)
        dataProvider = FetchedResultsDataProvider(fetchedResultsController: frc, delegate: self)
        dataSource = TableViewDataSource(tableView: tableView, dataProvider: dataProvider, delegate: self)
    }

    var selectedWaypoint: WayPoint?
    var selectedMapItem: MKMapItem?

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueIdentifierForSegue(segue: segue) {
        case .ShowWayPoint:
            guard let vc = segue.destination as? WayPointMapViewController else { fatalError("Wrong view controller type") }
            vc.managedObjectContext = managedObjectContext
            let points = WayPoint.wayPoints(onDate: selectedWaypoint!.createdAt!, from: managedObjectContext)//.map { return $0.location!  }
            vc.points = points
            vc.mapItem = selectedMapItem
        }
    }

}

extension WayPointsViewController : UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30.0
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let waypoint = dataSource.selectedObject!
        tableView.deselectRow(at: indexPath, animated: true)
        waypoint.location?.computeMapItem { (mapItem) in
            self.selectedMapItem = mapItem
            self.selectedWaypoint = waypoint
            self.performSegue(segueIdentifier:SegueIdentifier.ShowWayPoint)
        }
    }

}

extension WayPointsViewController: DataProviderDelegate {
    func dataProviderDidUpdate(updates: [DataProviderUpdate<WayPoint>]?) {
        dataSource.processUpdates(updates: updates)
    }
}

private let tableHeaderDateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .medium
    formatter.timeStyle = .none
    formatter.doesRelativeDateFormatting = true
    formatter.formattingContext = .standalone
    return formatter
}()

extension WayPointsViewController: DataSourceDelegate {

    internal func titleForHeaderInSection(section: Int) -> String? {
        let indexPath = IndexPath(row: 0, section: section)
        let object = dataProvider.objectAtIndexPath(indexPath: indexPath)
        let date = tableHeaderDateFormatter.string(from: object.createdAt!)
        return date
    }

    func cellIdentifierForObject(object: WayPoint) -> String {
        return "WayPointCell"
    }
}
