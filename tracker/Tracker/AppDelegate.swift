import UIKit
import CoreData
import TrackerModel
import CoreDataHelpers
import CoreLocation
import MapKit
import Chameleon
import QuickSettings
import FontAwesomeKit
import Fabric
import Crashlytics
import TrackerNetwork

extension AppDelegate : CLLocationManagerDelegate {

    // MARK: Tracker

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        updateTracker()
    }

    func updateTracker() {
        guard Settings.trackingEnabled else {
            tracker = nil
            return
        }

        let predicate = NSPredicate(format: "name = %@", "Home")
        let home = NamedLocation.findOrFetchInContext(managedObjectContext, matchingPredicate: predicate)
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways:
            tracker = TrackerController(moc:managedObjectContext, referenceLocation:home)
        case .notDetermined:
            if manager == nil {
                manager = CLLocationManager()
                manager?.delegate = self
                manager?.requestAlwaysAuthorization()
            }
        case .authorizedWhenInUse, .restricted, .denied:
            let alertController = UIAlertController(
                title: "Background Location Access Disabled",
                message: "In order to be notified about adorable kittens near you, please open "
                    + "this app's settings and set location access to 'Always'.",
                preferredStyle: .alert)

            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)

            let openAction = UIAlertAction(title: "Open Settings", style: .default) { (_) in
                if let url = URL(string:UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }
            alertController.addAction(openAction)

            window?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }

}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UINavigationControllerDelegate, QSSettingsViewControllerDelegate {

    var window: UIWindow?
    var tracker: TrackerController?
    var network: Network!
    var manager: CLLocationManager?

    lazy var appData: AppData = {
        return AppDataSQLite()
    }()

    func settingsViewController(settingsVc _: QSSettingsViewController, didUpdateSetting _: String) {
        updateTracker()
    }

    var managedObjectContext: NSManagedObjectContext {
        return appData.viewContext
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        appData.saveContext()
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        network = Network(URL(string: "http://127.0.0.1:8000")!)
        guard let vc = window?.rootViewController as? ManagedObjectContextSettable else { fatalError("Wrong view controller type") }
        vc.managedObjectContext = managedObjectContext
        guard let tabs = window?.rootViewController as? UITabBarController else { fatalError("what the") }
        let settingsVC: UINavigationController = {
            let vc = QSSettingsViewController(root:trackerSettings, delegate:self)
            vc.tabBarItem.icon = FAKMaterialIcons.settingsIcon(withSize: 30.0)
            vc.tabBarItem.title = "Settings"
            vc.title = "Settings"

            return UINavigationController(rootViewController: vc)
        }()

        let accountVC: UINavigationController = {
            let vc = AccountViewController()
            vc.managedObjectContext = managedObjectContext
            vc.network = network
            return TrackerNavigationController(rootViewController: vc)
        }()

        let activityVC: UINavigationController = {
            let vc = ActivityRootViewController()
            vc.managedObjectContext = managedObjectContext
            vc.tabBarItem.icon = FAKMaterialIcons.chartIcon(withSize: 30.0)
            vc.tabBarItem.title = "Activity"
            vc.title = "Activity"

            return UINavigationController(rootViewController: vc)
        }()

        tabs.viewControllers?.insert(activityVC, at: 0)
        tabs.viewControllers?.append(settingsVC)
        tabs.viewControllers?.append(accountVC)

        Fabric.with([Crashlytics.self])

        _ = TrackerNotification.addObserver(.saveRequested) {
            self.save()
        }

        //Chameleon.setGlobalThemeUsingPrimaryColor(UIColor.flatPlum(), with: .contrast)

        // Setup tracker
        updateTracker()

        return true
    }

    public func save() {
        appData.saveContext()
    }

}
