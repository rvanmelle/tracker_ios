import UIKit
import MapKit
import CoreLocation
import CoreData
import FontAwesomeKit

class RouteViewController: UIViewController, ManagedObjectContextSettable {

    var managedObjectContext: NSManagedObjectContext!

    @IBOutlet weak var sourceField: UITextField!
    @IBOutlet weak var destinationField1: UITextField!
    @IBOutlet weak var destinationField2: UITextField!
    @IBOutlet weak var topMarginConstraint: NSLayoutConstraint!
    @IBOutlet var enterButtonArray: [UIButton]!

    var originalTopMargin: CGFloat!

    let locationManager = CLLocationManager()
    var locationTuples: [(textField: UITextField, mapItem: MKMapItem?)]!

    var locationsArray: [(textField: UITextField, mapItem: MKMapItem?)] {
        var filtered = locationTuples.filter({ $0.mapItem != nil })
        // the next line makes it a round trip: 
        filtered += [filtered.first!]
        return filtered
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        tabBarItem.icon = FAKMaterialIcons.carIcon(withSize: 30.0)
        tabBarItem.title = "Route"
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        locationTuples = [(sourceField, nil), (destinationField1, nil), (destinationField2, nil)]
        originalTopMargin = topMarginConstraint.constant
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
            locationManager.requestLocation()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
    }

    @IBAction func getDirections(_ sender: AnyObject) {
        view.endEditing(true)
        performSegue(withIdentifier: "show_directions", sender: self)
    }

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if locationTuples[0].mapItem == nil ||
            (locationTuples[1].mapItem == nil && locationTuples[2].mapItem == nil) {
            showAlert("Please enter a valid starting point and at least one destination.")
            return false
        } else {
            return true
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let directionsViewController = segue.destination as? DirectionsViewController else { return }
        directionsViewController.locationArray = locationsArray
    }

    @IBAction func addressEntered(_ sender: UIButton) {
        view.endEditing(true)
        let currentTextField = locationTuples[sender.tag-1].textField
        CLGeocoder().geocodeAddressString(currentTextField.text!,
                                          completionHandler: {(placemarks, _) in
                                            if let placemarks = placemarks {
                                                var addresses = [String]()
                                                for placemark in placemarks {
                                                    addresses.append(placemark.formattedAddress)
                                                }
                                                self.showAddressTable(addresses: addresses, textField: currentTextField,
                                                                      placemarks: placemarks, sender: sender)
                                            } else {
                                                self.showAlert("Address not found.")
                                            }
        })
    }

    @IBAction func swapFields(_ sender: AnyObject) {
        swap(&destinationField1.text, &destinationField2.text)
        swap(&locationTuples[1].mapItem, &locationTuples[2].mapItem)
        swap(&self.enterButtonArray.filter { $0.tag == 2 }.first!.isSelected, &self.enterButtonArray.filter { $0.tag == 3 }.first!.isSelected)
    }

    func showAddressTable(addresses: [String], textField: UITextField, placemarks: [CLPlacemark], sender: UIButton) {

        let addressTableView = AddressTableView(frame: UIScreen.main.bounds, style: UITableViewStyle.plain)
        addressTableView.addresses = addresses
        addressTableView.currentTextField = textField
        addressTableView.placemarkArray = placemarks
        addressTableView.mainViewController = self
        addressTableView.sender = sender
        addressTableView.delegate = addressTableView
        addressTableView.dataSource = addressTableView
        view.addSubview(addressTableView)
    }

    func showAlert(_ alertString: String) {
        let alert = UIAlertController(title: nil, message: alertString, preferredStyle: .alert)
        let okButton = UIAlertAction(title: "OK",
                                     style: .cancel) { (_) -> Void in
        }
        alert.addAction(okButton)
        present(alert, animated: true, completion: nil)
    }

    // The remaining methods handle the keyboard resignation/
    // move the view so that the first responders aren't hidden

    func moveViewUp() {
        if topMarginConstraint.constant != originalTopMargin {
            return
        }

        topMarginConstraint.constant -= 165
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
    }

    func moveViewDown() {
        if topMarginConstraint.constant == originalTopMargin {
            return
        }

        topMarginConstraint.constant = originalTopMargin
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.layoutIfNeeded()
        })
  }
}

extension RouteViewController: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        enterButtonArray.filter {$0.tag == textField.tag}.first!.isSelected = false
        locationTuples[textField.tag-1].mapItem = nil
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        moveViewUp()
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        moveViewDown()
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        moveViewDown()
        return true
    }
}

extension RouteViewController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        CLGeocoder().reverseGeocodeLocation(locations.last!) { (placemarks, _) in
                if let placemarks = placemarks {
                    let placemark = placemarks[0]
                    self.locationTuples[0].mapItem = MKMapItem(placemark:
                        MKPlacemark(coordinate: placemark.location!.coordinate,
                                    addressDictionary: placemark.addressDictionary as? [String:AnyObject]))
                    self.sourceField.text = placemark.formattedAddress
                    self.enterButtonArray.filter { $0.tag == 1 }.first!.isSelected = true
                }
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }

}
