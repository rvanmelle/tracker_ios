//
//  Location.swift
//  Tracker
//
//  Created by Reid van Melle on 2016-10-18.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

import CoreDataHelpers
import CoreLocation
import CoreData

public class NamedLocation: ManagedObject {

    @NSManaged public var location: CLLocation?
    @NSManaged public var name: String?
    @NSManaged public var uid: String?
    @NSManaged public var createdAt: Date?
    @NSManaged public var radius: Float
    @NSManaged public var address: String?
    @NSManaged public var pointOfInterest: String?
}

extension CLPlacemark {
    // ABCreateStringWithAddressDictionary ? AddressBookUI
    var formattedAddress: String {
        guard let dict = addressDictionary, let components = dict["FormattedAddressLines"] as? [String] else {
            return ""
        }
        return components.joined(separator: ", ")
    }
}

extension NamedLocation: ManagedObjectType {
    public static var entityName: String { return "NamedLocation" }

    public static var defaultSortDescriptors: [NSSortDescriptor] {
        return [NSSortDescriptor(key: "createdAt", ascending: false)]
    }

    public static var defaultPredicate: NSPredicate? {
        return nil
    }

    public class func create(fromPlacemark placemark: CLPlacemark,
                             named name: String, inContext context: NSManagedObjectContext) -> NamedLocation {
        let loc: NamedLocation = context.insertObject()
        loc.name = name
        loc.location = placemark.location
        loc.pointOfInterest = placemark.areasOfInterest?.first ?? placemark.name
        loc.address = placemark.formattedAddress
        loc.radius = 100.0
        loc.uid = UUID().uuidString
        loc.createdAt = Date()

        return loc
    }
}
