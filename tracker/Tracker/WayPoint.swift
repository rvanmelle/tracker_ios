//
//  File.swift
//  Tracker
//
//  Created by Reid van Melle on 2016-10-19.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

import Foundation
import CoreDataHelpers
import CoreLocation
import CoreData
import Timepiece
import TrackerKit

public enum ActivityType: String {
    case walking
    case running
    case stationary
    case automotive
    case cycling
    case unknown
}

private let gregorianCalendar = Calendar(identifier: .gregorian)

extension Date {

    var normalizedDate: Date? {
        let dateComponents = gregorianCalendar.dateComponents([.day, .month, .year], from: self)
        return gregorianCalendar.date(from: dateComponents)
    }

}

public class WayPoint: ManagedObject {

    /*@nonobjc public class func fetchRequest() -> NSFetchRequest<WayPoint> {
        return NSFetchRequest<WayPoint>(entityName: "WayPoint");
    }*/

    @NSManaged public var location: CLLocation?
    @NSManaged public var createdAt: Date?
    @NSManaged public var lastTrackedAt: Date?

    @NSManaged public var address: String?
    @NSManaged public var pointOfInterest: String?
    @NSManaged public var locationUID: String?
    @NSManaged public var distance: Float
    @NSManaged public var hits: Int32
    @NSManaged public var trackingMode: String?
    @NSManaged private var activity: String?

    public var normalizedDate: Date? {
        let normalizedDateKey = "normalizedDate"
        willAccessValue(forKey: normalizedDateKey)
        let result = primitiveValue(forKey: normalizedDateKey) as? Date
        didAccessValue(forKey: normalizedDateKey)
        if let result = result {
            return result
        } else {
            let newValue = createdAt?.normalizedDate
            willChangeValue(forKey: normalizedDateKey)
            setPrimitiveValue(newValue, forKey: normalizedDateKey)
            didChangeValue(forKey: normalizedDateKey)
            return newValue
        }
    }

    public var lastVisitedAt: Date {
        return lastTrackedAt ?? createdAt!
    }

    public var firstVisitedAt: Date {
        return createdAt!
    }

    public var duration: TimeInterval {
        return lastVisitedAt.timeIntervalSince(firstVisitedAt)
    }

    public var activityType: ActivityType? {
        get {
            guard let a = activity else {
                return nil
            }
            return ActivityType(rawValue: a)
        }
        set {
            activity = newValue?.rawValue
        }
    }

    internal class func requestForWayPoints(onDate: Date, from context: NSManagedObjectContext) -> NSFetchRequest<WayPoint> {
        let start = onDate.beginningOfDay
        let end = start + 1.day
        let pred = NSPredicate(format: "createdAt > %@ AND createdAt < %@", start as NSDate, end! as NSDate)
        guard let request = WayPoint.sortedFetchRequestWithPredicate(pred) as? NSFetchRequest<WayPoint> else { fatalError() }
        request.sortDescriptors = [NSSortDescriptor(key: "createdAt", ascending: true)]
        return request
    }

    public class func wayPoints(onDate: Date, from context: NSManagedObjectContext) -> [WayPoint] {
        let request = requestForWayPoints(onDate:onDate, from:context)
        do {
            return try context.fetch(request)
        } catch {
            return []
        }
    }

    public class func firstWayPoint(previousToDate: Date, from context: NSManagedObjectContext) -> WayPoint? {
        let start = previousToDate.beginningOfDay
        let pred = NSPredicate(format: "createdAt < %@", start as NSDate)
        guard let request = WayPoint.sortedFetchRequestWithPredicate(pred) as? NSFetchRequest<WayPoint> else { fatalError() }
        request.sortDescriptors = [NSSortDescriptor(key: "createdAt", ascending: false)]
        request.fetchLimit = 1
        do {
            let points = try context.fetch(request)
            assert(points.count <= 1)
            return points.first
        } catch {
            return nil
        }
    }

    public class func nextWayPoint(afterDate: Date, from context: NSManagedObjectContext) -> WayPoint? {
        guard let start = afterDate.beginningOfDay + 1.day else { return nil }
        let pred = NSPredicate(format: "createdAt > %@", start as NSDate)
        guard let request = WayPoint.sortedFetchRequestWithPredicate(pred) as? NSFetchRequest<WayPoint> else { fatalError() }
        request.sortDescriptors = [NSSortDescriptor(key: "createdAt", ascending: true)]
        request.fetchLimit = 1
        do {
            let points = try context.fetch(request)
            assert(points.count <= 1)
            return points.first
        } catch {
            return nil
        }
    }

    public class func countOfWayPoints(onDate: Date, from context: NSManagedObjectContext) -> Int {
        let request = requestForWayPoints(onDate:onDate, from:context)
        do {
            return try context.count(for: request)
        } catch {
            return 0
        }
    }

}

extension WayPoint: ManagedObjectType {

    public static var entityName: String { return "WayPoint" }

    public static var defaultSortDescriptors: [NSSortDescriptor] {
        return [NSSortDescriptor(key: "createdAt", ascending: false)]
    }

    public static var defaultPredicate: NSPredicate? {
        return NSPredicate(value:true)
    }
}
