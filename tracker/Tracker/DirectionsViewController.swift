import UIKit
import MapKit
import CoreLocation

struct Route {
    let startingAddress: String
    let endingAddress: String
    let route: MKRoute
}

class DirectionsViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var totalTimeLabel: UILabel!
    @IBOutlet weak var directionsTableView: DirectionsTableView!

    var activityIndicator: UIActivityIndicatorView?
    var locationArray: [(textField: UITextField, mapItem: MKMapItem?)]!

    override func viewDidLoad() {
        super.viewDidLoad()
        directionsTableView.contentInset = UIEdgeInsets(top: -35, left: 0, bottom: -20, right: 0)

        addActivityIndicator()
        calculateSegmentDirections(index: 0, time: 0, routes: [])
    }

    func addActivityIndicator() {
        activityIndicator = UIActivityIndicatorView(frame: UIScreen.main.bounds)
        activityIndicator?.activityIndicatorViewStyle = .whiteLarge
        activityIndicator?.backgroundColor = view.backgroundColor
        activityIndicator?.startAnimating()
        view.addSubview(activityIndicator!)
    }

    func hideActivityIndicator() {
        if activityIndicator != nil {
            activityIndicator?.removeFromSuperview()
            activityIndicator = nil
        }
    }

    func calculateSegmentDirections(index: Int, time: TimeInterval, routes: [MKRoute]) {
        let request: MKDirectionsRequest = MKDirectionsRequest()
        request.source = locationArray[index].mapItem
        request.destination = locationArray[index+1].mapItem
        request.requestsAlternateRoutes = true
        request.transportType = .automobile
        let directions = MKDirections(request: request)
        /*directions.calculateETA { (etaResponse, error) in
            if let eta = etaResponse {
                print("travel time: \(eta.expectedTravelTime)")
            }
        }*/
        directions.calculate { (response, error) in
            if let routeResponse = response?.routes {
                let quickestRouteForSegment: MKRoute = routeResponse.sorted(by: {$0.expectedTravelTime < $1.expectedTravelTime})[0]
                var timeVar = time
                var routesVar = routes
                routesVar.append(quickestRouteForSegment)
                timeVar += quickestRouteForSegment.expectedTravelTime
                if index+2 < self.locationArray.count {
                    self.calculateSegmentDirections(index: index+1, time: timeVar, routes: routesVar)
                } else {
                    self.hideActivityIndicator()
                    self.showRoute(routes: routesVar, time: timeVar)
                }
            } else if let _ = error {
                let alert = UIAlertController(title: nil, message: "Directions not available.", preferredStyle: .alert)
                let okButton = UIAlertAction(title: "OK",
                                             style: .cancel) { (_) -> Void in
                                                _ = self.navigationController?.popViewController(animated: true)
                }
                alert.addAction(okButton)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    func printTimeToLabel(time: TimeInterval) {
        let timeString = time.formatted()
        totalTimeLabel.text = "Total Time: \(timeString)"
    }

    func displayDirections(directionsArray: [Route]) {
        directionsTableView.directionsArray = directionsArray
        directionsTableView.delegate = directionsTableView
        directionsTableView.dataSource = directionsTableView
        directionsTableView.reloadData()
    }

    func showRoute(routes: [MKRoute], time: TimeInterval) {
        var directionsArray = [Route]()
        for i in 0..<routes.count {
            plotPolyline(route: routes[i])
            let startingAddress = locationArray[i].textField.text!
            let endingAddress = locationArray[i+1].textField.text!
            let newElement = Route(startingAddress: startingAddress, endingAddress: endingAddress, route: routes[i])
            directionsArray.append(newElement)
        }
        displayDirections(directionsArray: directionsArray)
        printTimeToLabel(time: time)
    }

    func plotPolyline(route: MKRoute) {
        mapView.add(route.polyline)
        if mapView.overlays.count == 1 {
            mapView.setVisibleMapRect(route.polyline.boundingMapRect,
                                      edgePadding: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10),
                                      animated: false)
        } else {
            let polylineBoundingRect =  MKMapRectUnion(mapView.visibleMapRect,
                                                       route.polyline.boundingMapRect)
            mapView.setVisibleMapRect(polylineBoundingRect,
                                      edgePadding: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10),
                                      animated: false)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = false
        automaticallyAdjustsScrollViewInsets = false
    }
}

extension DirectionsViewController: MKMapViewDelegate {

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {

        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        if overlay is MKPolyline {
            if mapView.overlays.count == 1 {
                polylineRenderer.strokeColor =
                    UIColor.blue.withAlphaComponent(0.75)
            } else if mapView.overlays.count == 2 {
                polylineRenderer.strokeColor =
                    UIColor.green.withAlphaComponent(0.75)
            } else if mapView.overlays.count == 3 {
                polylineRenderer.strokeColor =
                    UIColor.red.withAlphaComponent(0.75)
            }
            polylineRenderer.lineWidth = 5
        }
        return polylineRenderer    }
}
