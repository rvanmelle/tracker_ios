//
//  ConfigurableCell.swift
//  Tracker
//
//  Created by Reid van Melle on 2016-10-19.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

protocol ConfigurableCell {
    associatedtype DataSource
    func configureForObject(object: DataSource)
}
