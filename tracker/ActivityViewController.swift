//
//  DataViewController.swift
//  pager
//
//  Created by Reid van Melle on 2016-11-02.
//  Copyright © 2016 Reid van Melle. All rights reserved.
//

import UIKit

class ActivityViewController: UIViewController {

    let dataLabel = UILabel()
    var dataObject: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        dataLabel.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.lightGray
        view.addSubview(dataLabel)
        view.pinItemCenter(dataLabel, to: view)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.dataLabel.text = dataObject
        self.dataLabel.sizeToFit()
    }

}
