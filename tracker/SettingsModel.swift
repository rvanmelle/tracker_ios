//
//  SettingsModel.swift
//  Tracker
//
//  Created by Reid van Melle on 2016-10-29.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

import Foundation
import QuickSettings
import CoreLocation

enum SettingDefinitions: String {
    case trackingEnabled = "tracking.trackingEnabled"
    case trackingMode    = "tracking.mode"
    case trackingAccuracy = "tracking.accuracy"
}

struct Settings {

    static let defaults = UserDefaults.standard

    static var trackingEnabled: Bool {
        return defaults.bool(forKey: SettingDefinitions.trackingEnabled.rawValue)
    }

    static var trackingMode: TrackerModeSetting {
        if let value = defaults.string(forKey: SettingDefinitions.trackingMode.rawValue) {
            return TrackerModeSetting(rawValue: value) ?? .DistanceFilter
        } else {
            return .DistanceFilter
        }
    }

    static var trackingAccuracy: TrackerAccuracy {
        if let value = defaults.string(forKey: SettingDefinitions.trackingAccuracy.rawValue) {
            return TrackerAccuracy(rawValue: value) ?? .Good
        } else {
            return .Good
        }
    }
}

enum TrackerModeSetting: String, QSDescriptionEnum {
    case SignificantLocationChange = "Significant Location Change"
    case Fitness
    case DistanceFilter = "Distance Filter"
    case Visits

    var description: String? {
        switch self {
        case .SignificantLocationChange:
            return "Returns a new location point whenever the system determines that a significant location change has taken place."
        case .DistanceFilter:
            return "Returns a new location point whenever a distance threshold has been exceeded."
        default:
            return nil
        }
    }
}

let trackerModeOptions = QSEnumSettingsOptions<TrackerModeSetting>(defaultValue: .DistanceFilter)

enum TrackerAccuracy: String, QSDescriptionEnum {
    case Best
    case Good
    case Coarse

    var updateDistance: CLLocationDistance {
        switch self {
        case .Best: return 25
        case .Good: return 250
        case .Coarse:  return 2000
        }
    }

    var locationAccuracy: CLLocationAccuracy {
        switch self {
        case .Best: return kCLLocationAccuracyBest
        case .Good: return kCLLocationAccuracyHundredMeters
        case .Coarse: return kCLLocationAccuracyThreeKilometers
        }
    }

    var description: String? {
        switch self {
        case .Best:
            return "Uses the highest accuracy tracking available on the device."
        case .Good:
            return "Try to track location to the nearest 100 meters."
        case .Coarse:
            return "Try to track location to the nearest 3 kilometers."
        }
    }
}

let accuracyOptions = QSEnumSettingsOptions<TrackerAccuracy>(defaultValue: .Good)

private let settings = [
    QSGroup(title: "Tracking", children: [
        QSToggle(label: "Enable tracking", key: "tracking.trackingEnabled", defaultValue: true),
        QSSelect(label: "Mode", key: "tracking.mode", options: trackerModeOptions),
        QSSelect(label: "Accuracy", key: "tracking.accuracy", options: accuracyOptions)
        ]
    )
]
let trackerSettings = QSGroup(title: "Settings", children: settings, footer:nil)
