//
//  Conf.swift
//  Tracker
//
//  Created by Reid van Melle on 2016-11-10.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

import Foundation

struct Conf {

    static var segmentSeparatorTimeInterval: TimeInterval = 15*60

}
