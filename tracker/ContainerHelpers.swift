//
//  ContainerHelpers.swift
//  Tracker
//
//  Created by Reid van Melle on 2017-04-04.
//  Copyright © 2017 Reid van Melle Inc. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class TrackerNavigationController: UINavigationController {

    override var tabBarItem: UITabBarItem! {
        get {
            return self.topViewController?.tabBarItem
        }
        set {
        }
    }

}

extension UINavigationController : ManagedObjectContextSettable {

    var managedObjectContext: NSManagedObjectContext! {
        get {
            guard let root = topViewController as? ManagedObjectContextSettable else { fatalError() }
            return root.managedObjectContext
        }
        set {
            guard let root = topViewController as? ManagedObjectContextSettable else { fatalError() }
            root.managedObjectContext = newValue
        }
    }

}

extension UITabBarController : ManagedObjectContextSettable {
    var managedObjectContext: NSManagedObjectContext! {
        get {
            guard let _vc = viewControllers?.first, let vc = _vc as? ManagedObjectContextSettable else { fatalError() }
            return vc.managedObjectContext
        }
        set {
            guard let vcs = viewControllers else { return }
            for _vc in vcs {
                guard let vc = _vc as? ManagedObjectContextSettable else { continue }
                vc.managedObjectContext = newValue
            }
        }
    }
}
