import UIKit

class CollectionViewDataSource<Delegate: DataSourceDelegate, Data: DataProvider, Cell: UICollectionViewCell>:
    NSObject, UICollectionViewDataSource where Delegate.Object == Data.Object,
    Cell: ConfigurableCell, Cell.DataSource == Data.Object {

    required init(collectionView: UICollectionView, dataProvider: Data, delegate: Delegate) {
        self.collectionView = collectionView
        self.dataProvider = dataProvider
        self.delegate = delegate
        super.init()
        collectionView.dataSource = self
        collectionView.reloadData()
    }

    var selectedObject: Data.Object? {
        guard let indexPath = collectionView.indexPathsForSelectedItems?.first else { return nil }
        return dataProvider.objectAtIndexPath(indexPath: indexPath)
    }

    func processUpdates(updates: [DataProviderUpdate<Data.Object>]?) {
        guard let updates = updates else { return collectionView.reloadData() }
        collectionView.performBatchUpdates({
            for update in updates {
                switch update {
                case .Insert(let indexPath):
                    self.collectionView.insertItems(at: [indexPath])
                case .Update(let indexPath, let object):
                    guard let cell = self.collectionView.cellForItem(at: indexPath) as? Cell else {
                        fatalError("wrong cell type")
                    }
                    cell.configureForObject(object: object)
                case .Move(let indexPath, let newIndexPath):
                    self.collectionView.deleteItems(at: [indexPath])
                    self.collectionView.insertItems(at: [newIndexPath])
                case .Delete(let indexPath):
                    self.collectionView.deleteItems(at: [indexPath])
                }
            }
        }, completion: nil)
    }

    // MARK: Private

    private let collectionView: UICollectionView
    private let dataProvider: Data
    private weak var delegate: Delegate!

    // MARK: UICollectionViewDataSource

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataProvider.numberOfItemsInSection(section: section)
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let object = dataProvider.objectAtIndexPath(indexPath: indexPath)
        let identifier = delegate.cellIdentifierForObject(object: object)
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier,
                                                            for: indexPath) as? Cell else {
            fatalError("Unexpected cell type at \(indexPath)")
        }
        cell.configureForObject(object: object)
        return cell
    }

}
