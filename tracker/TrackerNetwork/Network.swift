//
//  Network.swift
//  Tracker
//
//  Created by Reid van Melle on 2017-03-30.
//  Copyright © 2017 Reid van Melle Inc. All rights reserved.
//

import Foundation
import Alamofire
import KeychainSwift

enum Path: String {
    case authToken = "api-token-auth"
}

public class Network {

    enum Status {
        case loggedIn(String)
        case loggedOut
    }

    let baseURL: URL
    let keychain = KeychainSwift()
    let interface: Interface
    private var status: Status = Status.loggedOut

    enum Constant {
        static let authKeychainKey = "auth-token"
    }

    public init(_ baseURL: URL) {
        self.baseURL = baseURL

        if let token = keychain.get(Constant.authKeychainKey) {
            status = Status.loggedIn(token)
            interface = AlamofireInterface(baseURL.absoluteString, token: token)
        } else {
            interface = AlamofireInterface(baseURL.absoluteString)
        }
    }

    public var loggedIn: Bool {
        if case .loggedIn = status {
            return true
        } else {
            return false
        }
    }

    public func login(username: String, password: String) {
        // curl -X POST -H "Content-Type: application/json" -d '{"username":"admin","password":"password"}' http://127.0.0.1:8000/api-token-auth/
        let parameters: Parameters = [
            "username": username,
            "password": password
        ]
        interface.post(.authToken, parameters: parameters).responseJSON { (response) in
            switch response.result {
            case .success:
                print("Validation Successful")
                print("Response String: \(response.result.value!)")
                if let data = response.result.value as? [String:String], let token=data["token"] {
                    self.keychain.set(token, forKey: Constant.authKeychainKey)
                    self.status = Status.loggedIn(token)
                }
            case .failure(let error):
                print(error)
            }
        }
    }

    public func logout() {
        keychain.delete(Constant.authKeychainKey)
    }

}
