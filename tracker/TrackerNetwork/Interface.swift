//
//  Interface.swift
//  Tracker
//
//  Created by Reid van Melle on 2017-04-01.
//  Copyright © 2017 Reid van Melle Inc. All rights reserved.
//

import Foundation
import Alamofire

protocol Request {
    func responseJSON(queue: DispatchQueue?, options: JSONSerialization.ReadingOptions,
                      completionHandler: @escaping (Alamofire.DataResponse<Any>) -> Void) -> Self
}

protocol Interface {
    func post(_ path: Path, parameters: Parameters) -> Alamofire.DataRequest
}

class AlamofireInterface: Interface {

    let base: String
    var token: String?

    public init(_ base: String, token: String? = nil) {
        self.base = base
        self.token = token
    }

    func post(_ path: Path, parameters: Parameters) -> DataRequest {
        let fullPath = base + "/" + path.rawValue + "/"
        return Alamofire.request(fullPath, method: .post, parameters: parameters)
    }
}
