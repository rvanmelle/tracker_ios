import Foundation
import CoreLocation
import MapKit

protocol GeoLocationControllerDelegate: class {
    func geoLocationDidChangeAuthorizationStatus(authorized: Bool)
    func geoLocationDidChangeLocation(location: CLLocation)
    func geoLocationDidEnterRegion(region: CLRegion)
    func geoLocationDidExitRegion(region: CLRegion)
}

class GeoLocationController: NSObject {

    /**
        Energy Efficiency: https://developer.apple.com/library/content/documentation/Performance/Conceptual/EnergyGuide-iOS/LocationBestPractices.html
     
        There are a number of techniques which we can use and/or combine:
     
            - significant location change: this may work for tracking the approximate location of somebody
                 very roughly, but not nearly enough for tracking movements. the location updates can be
                 several kilometers apart
     
            - deferred updates with a time and/or distance limit: this is still a technique I'd like
                 invest more time in... at the time of my investigation, the deferred updates were
                 not working properly on iOS10
     
            - distance filter: this seems to be the most effective technique because we can tune the 
                 accuracy to control how often we need to get woken up to record points. this makes it
                 very effective for trading off power vs accuracy and tuning for biking vs running vs auto
     
            - region monitoring: this gives us triggers when we enter or leave important locations. In particular
                 we may be able to pause updates in the background and then use leaving a region as a trigger
                 to force tracking in the background and entering a region to re-enable this feature to
                 allow GPS tracking to go back to sleep
     
            - remote notifications: if we *only* need on-demand location tracking, then using a remote
                 trigger is another option
     
        WRT pausing updates in the background: we need to disable this because otherwise, we will stop
            tracking and not get woken back up
            
            http://stackoverflow.com/questions/17484352/iphone-gps-in-background-never-resumes-after-pause
     
            That being said, it would be interesting to try combining significant location change as a trigger
            to toggle "updates in the background" or the region monitoring as discussed above.
     
            The VISITS api is another potential trigger depending on how it interacts the "background updates"
            feature.
     
            We will also try pausing and unpausing when we enter pre-determined zones
     
    */
    enum TrackingMode: CustomStringConvertible {
        case significantChange
        case fitness(CLLocationAccuracy)
        case deferredUpdates(CLLocationDistance, Float)
        case distanceFilter(CLLocationDistance)
        case remotePoke
        case visits

        var description: String {
            switch self {
            case .significantChange: return "significantChange"
            case .fitness:           return "fitness"
            case .deferredUpdates:   return "deferred"
            case let .distanceFilter(val): return "filter(\(val))"
            case .remotePoke:        return "remote"
            case .visits:            return "visits"
            }
        }

        var isDeferredUpdates: Bool {
            switch self {
            case .deferredUpdates: return true
            default: return false
            }
        }

        var isDistanceFilter: Bool {
            switch self {
            case .distanceFilter: return true
            default: return false
            }
        }
    }

    var authorized: Bool {
        let status = CLLocationManager.authorizationStatus()
        return status == .authorizedAlways || status == .authorizedWhenInUse
    }

    var regionsToMonitor: [CLCircularRegion] = [] {
        didSet {
            for r in oldValue {
                locationManager.stopMonitoring(for: r)
            }
            if CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
                for r in regionsToMonitor {
                    locationManager.startMonitoring(for: r)
                }
            }
        }
    }

    fileprivate var deferringUpdates: Bool = false
    fileprivate var updatesReceived: Int = 0
    let trackingMode: TrackingMode

    required init(mode: TrackingMode, delegate: GeoLocationControllerDelegate) {
        self.trackingMode = mode
        super.init()
        self.delegate = delegate

        locationManager.delegate = self
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.activityType = .fitness

        let currentStatus = CLLocationManager.authorizationStatus()
        if currentStatus == .notDetermined || currentStatus == .authorizedWhenInUse {
            locationManager.requestAlwaysAuthorization()
            //requestWhenInUseAuthorization()
        } else {
            start()
        }
    }

    func retrieveCurrentLocation(completion: @escaping (CLLocation?, CLPlacemark?) -> Void) {
        guard let location = locationManager.location else {
            completion(nil, nil)
            return
        }
        guard previousLocation == nil || previousLocation!.distance(from: location) > 1000 || previousPlacemark == nil else {
            return completion(location, previousPlacemark)
        }
        geocoder.reverseGeocodeLocation(location) { placemarks, _ in
            self.previousPlacemark = placemarks?.first
            completion(location, placemarks?.first)
        }
    }

    // MARK: Private

    fileprivate func start() {
        delegate.geoLocationDidChangeAuthorizationStatus(authorized: authorized)

        guard authorized else { return }

        locationManager.distanceFilter = kCLDistanceFilterNone
        switch trackingMode {
        case .significantChange:
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startMonitoringSignificantLocationChanges()
        case let .fitness(accuracy):
            locationManager.desiredAccuracy = accuracy
            locationManager.startUpdatingLocation()
        case .deferredUpdates:
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        case let .distanceFilter(distance):
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = distance
            locationManager.startUpdatingLocation()
        case .visits:
            // http://nshipster.com/core-location-in-ios-8/
            fatalError()

        case .remotePoke:
            // https://blog.layer.com/how-we-leverage-ios-push-notifications/
            fatalError()
        }
    }

    fileprivate weak var delegate: GeoLocationControllerDelegate!
    private var locationManager: CLLocationManager = CLLocationManager()
    private var geocoder = CLGeocoder()
    private var previousLocation: CLLocation?
    private var previousPlacemark: CLPlacemark?

}

extension GeoLocationController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        manager.pausesLocationUpdatesAutomatically = true
        delegate.geoLocationDidEnterRegion(region: region)
    }

    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        manager.pausesLocationUpdatesAutomatically = false
        delegate.geoLocationDidExitRegion(region: region)
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        start()
    }

    func locationManager(_ manager: CLLocationManager, didFinishDeferredUpdatesWithError error: Error?) {
        if let err = error {
            print("Deferred location update error: \(err)")
        }
        deferringUpdates = false
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        delegate.geoLocationDidChangeLocation(location:locations.last!)
        updatesReceived += 1
        if trackingMode.isDeferredUpdates && !deferringUpdates && updatesReceived > 10 {
            //manager.allowDeferredLocationUpdates(untilTraveled: CLLocationDistanceMax, timeout: CLTimeIntervalMax)
            manager.allowDeferredLocationUpdates(untilTraveled: CLLocationDistanceMax, timeout: CLTimeIntervalMax)
            deferringUpdates = true
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location manager error: \(error)")
    }

}
