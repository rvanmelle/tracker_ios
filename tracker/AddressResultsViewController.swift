//
//  AddressResultsViewController.swift
//  Tracker
//
//  Created by Reid van Melle on 2016-10-21.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import TrackerKit

class AddressTableViewCell: UITableViewCell {}
extension AddressTableViewCell : ConfigurableCell {

    typealias DataSource = CLPlacemark

    func configureForObject(object: DataSource) {
        self.textLabel!.text = object.formattedAddress
        self.detailTextLabel?.text = object.areasOfInterest?.first ?? object.name
    }
}

protocol SimpleDataProviderDelegate: class {

    associatedtype Object : Any
    func dataProviderDidUpdate(contents: [Object])
}

class AddressSearchDataProvider<Delegate: SimpleDataProviderDelegate>: NSObject, DataProvider
where Delegate.Object == CLPlacemark {

    typealias Object = CLPlacemark

    var moc: NSManagedObjectContext!
    private weak var delegate: Delegate!

    required init(delegate: Delegate) {
        self.delegate = delegate
        super.init()
    }

    private var addresses: [CLPlacemark] = []

    var searchText: String? = nil {
        didSet {
            updateSearchResults()
        }
    }

    private func performSearch() {
        guard let text = searchText else { return }
        CLGeocoder().geocodeAddressString(text,
                                          completionHandler: {(placemarks, _) in
                                            if let placemarks = placemarks {
                                                self.addresses = placemarks
                                                self.delegate.dataProviderDidUpdate(contents: self.addresses)
                                            }
        })
    }
    private lazy var updateSearchResults : () -> Void = debounce(0.25, action:self.performSearch)

    func objectAtIndexPath(indexPath: IndexPath) -> Object {
        return addresses[indexPath.row]
    }

    func numberOfItemsInSection(section: Int) -> Int {
        return addresses.count
    }

    func deleteObjectAtIndexPath(indexPath: IndexPath) {
        fatalError()
    }

    var numberOfSections: Int? { return nil }
    func titleForHeaderInSection(section: Int) -> String? { return nil }
}

protocol AddressResultsDelegate : class {

    func addressResults(didSelectPlacemark placemark: CLPlacemark)

}

class AddressResultsViewController: UIViewController {

    let tableView = UITableView(frame: CGRect.zero, style: UITableViewStyle.plain)

    fileprivate typealias Data = AddressSearchDataProvider<AddressResultsViewController>
    fileprivate var dataProvider: Data!
    fileprivate var dataSource: TableViewDataSource<AddressResultsViewController, Data, AddressTableViewCell>!

    weak var delegate: AddressResultsDelegate?

    var searchText: String? = nil {
        didSet {
            dataProvider.searchText = searchText
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        view.pinItemFillAll(tableView)

        tableView.register(AddressTableViewCell.self, forCellReuseIdentifier: "AddressCell")
        dataProvider = AddressSearchDataProvider(delegate: self)
        dataSource = TableViewDataSource(tableView: tableView, dataProvider: dataProvider, delegate: self)

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        tableView.delegate = self
    }
}

extension AddressResultsViewController: SimpleDataProviderDelegate {
    func dataProviderDidUpdate(contents: [CLPlacemark]) {
        tableView.reloadData()
    }
}

extension AddressResultsViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let placemark = dataProvider.objectAtIndexPath(indexPath: indexPath)
        delegate?.addressResults(didSelectPlacemark: placemark)
    }
}

extension AddressResultsViewController: DataSourceDelegate {
    internal func titleForHeaderInSection(section: Int) -> String? {
        return nil
    }

    func cellIdentifierForObject(object: CLPlacemark) -> String {
        return "AddressCell"
    }
}
