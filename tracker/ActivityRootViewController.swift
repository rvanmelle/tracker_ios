//
//  RootViewController.swift
//  pager
//
//  Created by Reid van Melle on 2016-11-02.
//  Copyright © 2016 Reid van Melle. All rights reserved.
//

import UIKit

private let sharedDateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .medium
    formatter.timeStyle = .none
    formatter.doesRelativeDateFormatting = true
    formatter.formattingContext = .standalone
    return formatter
}()

class ActivityRootViewController: TrackerViewController {

    var pageViewController: UIPageViewController?

    fileprivate func configure(forViewController mapVC: WayPointMapViewController) {
        self.title = sharedDateFormatter.string(from: mapVC.date!)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        // Configure the page view controller and add it as a child view controller.
        self.pageViewController = UIPageViewController(transitionStyle: .scroll,
                                                       navigationOrientation: .horizontal, options: nil)
        self.pageViewController!.delegate = self

        //let startingViewController: ActivityViewController = self.modelController.viewControllerAtIndex(0)!
        let startingViewController = self.modelController.viewControllerForToday()
        configure(forViewController: startingViewController)
        let viewControllers = [startingViewController]
        self.pageViewController!.setViewControllers(viewControllers, direction: .forward,
                                                    animated: false, completion: {_ in })

        self.pageViewController!.dataSource = self.modelController

        self.addChildViewController(self.pageViewController!)
        self.view.addSubview(self.pageViewController!.view)

        // Set the page view controller's bounds using an inset rect so that self's view 
        // is visible around the edges of the pages.
        var pageViewRect = self.view.bounds
        if UIDevice.current.userInterfaceIdiom == .pad {
            pageViewRect = pageViewRect.insetBy(dx: 40.0, dy: 40.0)
        }
        self.pageViewController!.view.frame = pageViewRect

        self.pageViewController!.didMove(toParentViewController: self)

        view.backgroundColor = UIColor.flatBrown()
        self.pageViewController!.view.backgroundColor = ETColor.bg1

    }

    lazy var modelController: ActivityModelController = {
        return ActivityModelController(self.managedObjectContext)
    }()

}

extension ActivityRootViewController : UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool,
                            previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {

        if let vc = pageViewController.viewControllers?.last as? WayPointMapViewController {
            configure(forViewController: vc)
        }
    }
}
