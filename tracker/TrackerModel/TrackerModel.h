//
//  TrackerModel.h
//  TrackerModel
//
//  Created by Reid van Melle on 2016-10-19.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for TrackerModel.
FOUNDATION_EXPORT double TrackerModelVersionNumber;

//! Project version string for TrackerModel.
FOUNDATION_EXPORT const unsigned char TrackerModelVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TrackerModel/PublicHeader.h>


