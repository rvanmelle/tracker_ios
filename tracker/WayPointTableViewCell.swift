//
//  MoodTableViewCell.swift
//  Moody
//

import UIKit
import TrackerModel
import CoreLocation
import FontAwesomeKit

class WayPointTableViewCell: UITableViewCell {
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var distance: UILabel!
}

private let sharedDateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .medium
    formatter.timeStyle = .short
    formatter.doesRelativeDateFormatting = true
    formatter.formattingContext = .standalone
    return formatter
}()

extension WayPointTableViewCell: ConfigurableCell {
    typealias DataSource = WayPoint

    func configureForObject(object: WayPoint) {
        if let locUID = object.locationUID,
            let moc = object.managedObjectContext,
            let namedLocation = NamedLocation.fetchInContext(moc, configurationBlock: { (request) in
                request.fetchLimit = 1
                request.predicate = NSPredicate(format: "uid = %@", locUID)
            }).first {

            if namedLocation.name == "Home" {
                let icon = FAKMaterialIcons.homeIcon(withSize: 30)
                imageView?.image = icon?.image(with: CGSize(width: 30.0, height: 30.0))
            }
            address.text = namedLocation.name
            location.text = nil
            if object.hits > 1 {
                let timeInterval = Float(object.lastTrackedAt!.timeIntervalSince(object.createdAt!))
                distance.text = "\(object.hits) \(timeInterval.format(".1")))"
            }

        } else {

            address.text = object.pointOfInterest
            location.text = nil // object.pointOfInterest
            if object.hits > 1 {
                let timeInterval = Float(object.lastTrackedAt!.timeIntervalSince(object.createdAt!))
                distance.text = "\(object.hits) \(timeInterval.format(".1"))"
            } else {
                let date = sharedDateFormatter.string(from: object.createdAt!)
                let distanceText = Float(CLLocationDistance(object.distance) / 60.0).format(".1")
                let activity = object.activityType?.rawValue ?? ""
                distance.text = date + " " + distanceText  + " " + activity
            }
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        backgroundColor = UIColor.white
        address.text = nil
        location.text = nil
        distance.text = nil
        imageView?.image = nil
    }
}
