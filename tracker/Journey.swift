//
//  Journey.swift
//  Tracker
//
//  Created by Reid van Melle on 2016-11-10.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

import Foundation

public struct Journey: CustomStringConvertible {

    public let segments: [Segment]
    public let points: [WayPoint]

    public init(points: [WayPoint], separationInterval: TimeInterval) {
        self.points = points
        var segments: [Segment] = []
        var currentSegment: [WayPoint] = []
        var lastPoint  = points.first!
        for point in points {
            let interval = point.lastVisitedAt.timeIntervalSince(lastPoint.lastVisitedAt)
            if interval > separationInterval { //|| lastPoint.activityType! != point.activityType!  {
                segments.append(Segment(points: currentSegment))
                currentSegment = []
            }
            currentSegment.append(point)
            lastPoint = point
        }
        segments.append(Segment(points: currentSegment))
        self.segments = segments.filter({ (s) -> Bool in
            return s.points.count > 1 && s.distance >= 100.0
        })
    }

    public var description: String {
        var pieces: [String] = []
        pieces.append( "[Journey: points=\(points.count) segments=\(segments.count)]" )
        for s in segments {
            pieces.append("\t\(s.description)")
        }
        return pieces.joined(separator: "\n")
    }

}
