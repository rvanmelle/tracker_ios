//
//  Notifications.swift
//  Tracker
//
//  Created by Reid van Melle on 2016-10-22.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

import Foundation
import TrackerKit

extension TrackerNotification: TypedNotification {
    var content: TypedNotificationContentType? {
        switch self {
        case .saveRequested:
            return nil
        }
    }

    var name: String {
        return rawValue
    }

}

enum TrackerNotification: String {

    case saveRequested

    /*private var name: Notification.Name {
        return Notification.Name(rawValue: self.rawValue)
    }

    private var center: NotificationCenter {
        return NotificationCenter.default
    }

    func post(_ object: AnyObject? = nil, userInfo: [String: Any]? = nil) {
        if !Thread.isMainThread {
            DispatchQueue.main.async(execute: {
                self.center.post(name: self.name, object: object, userInfo: userInfo)
            })
        } else {
            center.post(name: self.name, object: object, userInfo: userInfo)
        }
    }

    func addObserver(_ observer: AnyObject, selector: Selector, object: AnyObject? = nil) {
        if !Thread.isMainThread {
            DispatchQueue.main.async(execute: {
                self.center.addObserver(observer, selector: selector, name: self.name, object: object)
            })
        } else {
            center.addObserver(observer, selector: selector, name: name, object: object)
        }
    }

    func removeObserver(_ observer: AnyObject, object: AnyObject? = nil) {
        if !Thread.isMainThread {
            DispatchQueue.main.async(execute: {
                self.center.removeObserver(observer, name: self.name, object: object)
            })
        } else {
            center.removeObserver(observer, name: name, object: object)
        }
    }

    public func addObserverForName(_ object: AnyObject? = nil, usingBlock : @escaping (Notification) -> Void) -> NSObjectProtocol {
        return center.addObserver(forName: name, object: object, queue: OperationQueue.main, using: usingBlock)
    }*/
}
