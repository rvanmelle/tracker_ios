import UIKit
import CoreData

protocol DataProvider: class {
    associatedtype Object: Any
    func objectAtIndexPath(indexPath: IndexPath) -> Object
    func numberOfItemsInSection(section: Int) -> Int
    var numberOfSections: Int? { get }
    func deleteObjectAtIndexPath(indexPath: IndexPath)
}

protocol DataProviderDelegate: class {
    associatedtype Object: NSFetchRequestResult
    func dataProviderDidUpdate(updates: [DataProviderUpdate<Object>]?)
}

enum DataProviderUpdate<Object> {
    case Insert(IndexPath)
    case Update(IndexPath, Object)
    case Move(IndexPath, IndexPath)
    case Delete(IndexPath)
}
