//
//  WayPointMapViewController.swift
//  Tracker
//
//  Created by Reid van Melle on 2016-10-20.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

import UIKit
import MapKit
import TrackerModel
import CoreDataHelpers
import Chameleon
import FontAwesomeKit

class JourneySegmentCell: UITableViewCell {
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: "JourneySegmentCell")
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    class func dequeue(tableView: UITableView, for indexPath: IndexPath) -> JourneySegmentCell! {
        return tableView.dequeueReusableCell(withIdentifier: "JourneySegmentCell", for: indexPath) as? JourneySegmentCell
    }

    class func register(tableView: UITableView) {
        tableView.register(JourneySegmentCell.self, forCellReuseIdentifier: "JourneySegmentCell")
    }
}

class WayPointMapViewController: TrackerViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView: UITableView!

    var date: Date?
    var mapItem: MKMapItem?
    var points: [WayPoint]?
    fileprivate var journey: Journey?
    fileprivate var selectedPolyline: MKPolyline?

    func plotPolyline(polyline: MKPolyline, selected: Bool=false) {
        if selected {
            selectedPolyline = polyline
        }
        mapView.add(polyline)
        if mapView.overlays.count == 1 {
            mapView.setVisibleMapRect(polyline.boundingMapRect,
                                      edgePadding: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10),
                                      animated: false)
        } else {
            let polylineBoundingRect =  MKMapRectUnion(mapView.visibleMapRect,
                                                       polyline.boundingMapRect)
            mapView.setVisibleMapRect(polylineBoundingRect,
                                      edgePadding: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10),
                                      animated: false)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        mapView.layer.cornerRadius = 10.0
        view.backgroundColor = ETColor.bg1
        setupTableview()

        // https://www.natashatherobot.com/peek-pop-view-inside-tableviewcell/

        /*if traitCollection.forceTouchCapability == .available {
            registerForPreviewingWithDelegate(self, sourceView: tableView)
        }*/

        if let mapItem = mapItem {
            let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
            let region = MKCoordinateRegion(center: mapItem.placemark.coordinate, span: span)
            mapView.setRegion(region, animated: true)

            let annotation = MKPointAnnotation()
            annotation.coordinate = mapItem.placemark.coordinate
            annotation.title = mapItem.placemark.formattedAddress
            mapView.addAnnotation(annotation)

            navigationItem.title = mapItem.name
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add Location", style: .plain,
                                                                target: self, action: #selector(createNamedLocation))
        }

        if let points = points, points.count > 0 {
            //print("Total Points: \(points.count)")
            journey = Journey(points: points, separationInterval: Conf.segmentSeparatorTimeInterval)
            print(journey!)
            mapView.delegate = self
            for segment in journey!.segments {
                //print(segment)
                plotPolyline(polyline: segment.polyline)
            }
        }
    }

    private func setupTableview() {
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        JourneySegmentCell.register(tableView: tableView)
    }

    func createNamedLocation() {
        let mapItem = self.mapItem!
        let alert = UIAlertController(title: "Create Location",
                                      message: "Please enter the nickname/short-hand for this location that you would like to use. eg. Home or Work",
                                      preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "Home"
        }
        alert.addAction(UIAlertAction(title: "Create", style: .default, handler: { (_) in
            guard let name = alert.textFields?.first?.text, name.characters.count > 0  else {
                return
            }
            let placemark = mapItem.placemark
            _ = NamedLocation.create(fromPlacemark: placemark, named: name, inContext: self.managedObjectContext)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }

}

extension WayPointMapViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let journey = journey else { return 0 }
        return journey.segments.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let journey = journey else { fatalError() }
        let segment = journey.segments[indexPath.row]
        guard let cell = JourneySegmentCell.dequeue(tableView: tableView, for: indexPath) else { fatalError() }
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.text = "\(segment.distance.kms())km in \(segment.duration.formatted(style:.positional))"
        cell.textLabel?.textColor = UIColor.white
        cell.detailTextLabel?.textColor = UIColor.lightGray
        let fromPlace = segment.points.first?.pointOfInterest
        let toPlace   = segment.points.last?.pointOfInterest
        cell.detailTextLabel?.text = "\(fromPlace!) TO \(toPlace!)"
        var icon: FAKIcon?
        switch segment.transportMode {
        case .automotive:
            icon = FAKMaterialIcons.carIcon(withSize: 30)
        case .cycling:
            icon = FAKMaterialIcons.bikeIcon(withSize: 30)
        case .running:
            icon = FAKMaterialIcons.runIcon(withSize: 30)
        case .walking:
            icon = FAKMaterialIcons.walkIcon(withSize: 30)
        case .stationary:
            icon = FAKMaterialIcons.stopIcon(withSize: 30)
        case .unknown:
            icon = FAKMaterialIcons.squareOIcon(withSize: 30)
        }
        icon?.addAttribute(NSForegroundColorAttributeName, value: UIColor.white)
        cell.imageView?.image = icon?.image(with: CGSize(width: 30, height: 30))
        return cell
    }
}

extension WayPointMapViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        mapView.removeOverlays(mapView.overlays)
        let segment = journey!.segments[indexPath.row]
        // [UIView animateWithDuration:0.3f
        //animations:^{
        //    myAnnotation.coordinate = newCoordinate;
        //}]
        plotPolyline(polyline: segment.polyline, selected:true)
    }

    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if let cell = tableView.cellForRow(at: indexPath), cell.isSelected {
            tableView.deselectRow(at: indexPath, animated: true)
            mapView.removeOverlays(mapView.overlays)
            for segment in journey!.segments {
                plotPolyline(polyline: segment.polyline)
            }
            return nil
        } else {
            return indexPath
        }
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    }
}

extension WayPointMapViewController: MKMapViewDelegate {

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {

        var lineWidth: CGFloat = 1.5
        if let selected = selectedPolyline, overlay === selected {
            lineWidth = 3.0
        }
        let colors = ColorSchemeOf(ColorScheme.triadic, color: UIColor.flatBlue(), isFlatScheme: true)
        /*let baseColor = UIColor.flatBlue()!
        let colors = [ baseColor,
                       baseColor.lighten(byPercentage: 0.1),
                       baseColor.lighten(byPercentage: 0.2),
                       baseColor.lighten(byPercentage: 0.3),
                       baseColor.lighten(byPercentage: 0.4),
                       baseColor.lighten(byPercentage: 0.5),
                       baseColor.lighten(byPercentage: 0.6),
                       baseColor.lighten(byPercentage: 0.7),
                       baseColor.lighten(byPercentage: 0.8)]*/
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        if overlay is MKPolyline {
            let strokeColor = colors[mapView.overlays.count % colors.count]
            polylineRenderer.strokeColor = strokeColor
            polylineRenderer.lineWidth = lineWidth
        }
        return polylineRenderer
    }
}
