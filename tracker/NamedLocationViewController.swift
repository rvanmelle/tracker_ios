//
//  NamedLocationViewController.swift
//  Tracker
//
//  Created by Reid van Melle on 2016-10-21.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import CoreDataHelpers
import TrackerModel
import FontAwesomeKit

extension UIImage {}

class NamedLocationTableViewCell: UITableViewCell { }

extension NamedLocationTableViewCell: ConfigurableCell {

    typealias DataSource = NamedLocation

    func configureForObject(object: NamedLocation) {
        textLabel?.text = object.name
        detailTextLabel?.text = object.pointOfInterest
        if object.name == "Home" {
            //imageView?.image = UIImage.fontAwesomeIcon(name: .home, textColor: UIColor.black, size: CGSize(width: 35, height: 35))
        }
        if object.name == "Work" {
            //imageView?.image = UIImage.fontAwesomeIcon(name: .bank, textColor: UIColor.black, size: CGSize(width: 35, height:35))
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        textLabel?.text = nil
        detailTextLabel?.text = nil
        imageView?.image = nil
    }
}

class NamedLocationViewController: TrackerViewController {

    @IBOutlet weak var tableView: UITableView!

    override func awakeFromNib() {
        super.awakeFromNib()
        tabBarItem.icon = FAKMaterialIcons.mapIcon(withSize: 30.0)
        tabBarItem.title = "Locations"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Locations"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(editLocations))
        setupTableView()
        setupSearch()
    }

    func editLocations() {
        tableView.isEditing = !tableView.isEditing
        navigationItem.rightBarButtonItem?.title = tableView.isEditing ? "Done" : "Edit"
    }

    // MARK: Private

    fileprivate typealias Data = FetchedResultsDataProvider<NamedLocationViewController>
    fileprivate var dataSource: TableViewDataSource<NamedLocationViewController, Data, NamedLocationTableViewCell>!

    private func setupTableView() {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        tableView.delegate = self
        guard let request = NamedLocation.sortedFetchRequest as? NSFetchRequest<NamedLocation> else {
            fatalError()
        }
        request.returnsObjectsAsFaults = false
        request.fetchBatchSize = 20
        let frc = NSFetchedResultsController<NamedLocation>(fetchRequest: request,
                                                            managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        let dataProvider = FetchedResultsDataProvider(fetchedResultsController: frc, delegate: self)
        dataSource = TableViewDataSource(tableView: tableView, dataProvider: dataProvider, delegate: self)
    }

    // MARK: Search Controller

    fileprivate var searchController: UISearchController!
    fileprivate var addressResults: AddressResultsViewController!

    private func setupSearch() {

        addressResults = AddressResultsViewController()
        addressResults.delegate = self
        searchController = UISearchController(searchResultsController: addressResults)

        // Use the current view controller to update the search results.
        searchController.searchResultsUpdater = self

        // Install the search bar as the table header.
        tableView.tableHeaderView = searchController.searchBar

        // It is usually good to set the presentation context.
        definesPresentationContext = true
        //searchDataSource = TableViewDataSource(tableView: tableView, dataProvider: searchDataProvider, delegate: self)
    }

}

extension NamedLocationViewController : AddressResultsDelegate {

    func addressResults(didSelectPlacemark placemark: CLPlacemark) {
        searchController.dismiss(animated: true) {
            let alert = UIAlertController(title: "Create Location",
                                          message: "Please enter the nickname/short-hand for this location " +
                                            "that you would like to use. eg. Home or Work", preferredStyle: .alert)
            alert.addTextField { (textField) in
                textField.placeholder = "Home"
            }
            alert.addAction(UIAlertAction(title: "Create", style: .default, handler: { (_) in
                guard let name = alert.textFields?.first?.text, name.characters.count > 0  else {
                    return
                }
                _ = NamedLocation.create(fromPlacemark: placemark, named: name, inContext: self.managedObjectContext)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension NamedLocationViewController : UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        addressResults.searchText = searchController.searchBar.text
    }

}

extension NamedLocationViewController : UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

}

extension NamedLocationViewController: DataProviderDelegate {
    func dataProviderDidUpdate(updates: [DataProviderUpdate<NamedLocation>]?) {
        dataSource.processUpdates(updates: updates)
    }

}

extension NamedLocationViewController: DataSourceDelegate {
    internal func titleForHeaderInSection(section: Int) -> String? {
        return nil
    }

    func cellIdentifierForObject(object: NamedLocation) -> String {
        return "NamedLocationCell"
    }
}
