//
//  AccountController.swift
//  Tracker
//
//  Created by Reid van Melle on 2017-04-03.
//  Copyright © 2017 Reid van Melle Inc. All rights reserved.
//

import Foundation
import TrackerKit
import QuickSettings
import TrackerNetwork
import FontAwesomeKit

class AccountViewController: TrackerViewController {

    var network: Network!

    lazy var settingsVC: QSSettingsViewController = {
        let vc = QSSettingsViewController(root:self.currentSettings, delegate:self)
        self.embedFillingChildVC(vc)
        return vc
    }()

    private var currentSettings: QSGroup {
        let account = [
            QSGroup(title: "Account", footer: nil) {
                return [
                    QSInfo(label: "Status", text: network.loggedIn ? "Logged In" : "Logged Out")
                ]
            },
            network.loggedIn ?
                QSGroup(title: nil, footer: "Your data will be securely kept for up to 180 days.") {
                    return [
                        QSAction(title: "Logout", actionType: .default, actionCallback: {})
                    ]
                }
                :
                QSGroup(title: nil, footer: "Create a free account to save your data.") {
                    return [
                        QSAction(title: "Sign In", actionType: .normal, actionCallback: {
                            let signInVC = SignInViewController()
                            signInVC.network = self.network
                            self.navigationController?.pushViewController(signInVC, animated: true)
                        }),
                        QSAction(title: "Create Account", actionType: .default, actionCallback: {})
                    ]
            }
        ]
        let accountSettings = QSGroup(title: "Account", children: account, footer:nil)
        return accountSettings
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        tabBarItem.icon = FAKMaterialIcons.accountIcon(withSize: 30.0)
        tabBarItem.title = "Account"
        title = "Account"
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        settingsVC.tableView.reloadData()
    }

}

extension AccountViewController: QSSettingsViewControllerDelegate {
    func settingsViewController(settingsVc: QSSettingsViewController, didUpdateSetting settingId: String) {

    }
}
