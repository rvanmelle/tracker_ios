//
//  Segment.swift
//  Tracker
//
//  Created by Reid van Melle on 2016-11-10.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit
import TrackerKit

public struct Segment: CustomStringConvertible {

    public let points: [WayPoint]

    public init(points: [WayPoint]) {
        self.points = points
    }

    public var description: String {
        return "[Segment: mode=\(transportMode.rawValue) points=\(points.count) distance=\(distance.kms()) " +
            "duration=\(duration.formatted(style:.positional))]"
    }

    public var transportMode: ActivityType {
        var modes: [ActivityType: Int] = [:]
        for p in points {
            if let pm = p.activityType {
                modes[pm, or:0] += 1
            }
        }
        let sorted = modes.sorted { (pair1, pair2) -> Bool in
            return pair1.value > pair2.value
        }
        return sorted[0].key
    }

    public var distance: CLLocationDistance {
        guard let firstPoint = points.first else { return 0.0 }

        var prevPoint = firstPoint
        var total: CLLocationDistance = 0.0
        for p in points.suffix(from: 1) {
            total += p.location!.distance(from: prevPoint.location!)
            prevPoint = p
        }
        return total
    }

    public var duration: TimeInterval {
        let firstPoint = points.first
        let lastPoint = points.last
        switch (firstPoint, lastPoint) {
        case let (first?, last?) where first != last:
            return last.lastVisitedAt.timeIntervalSince(first.firstVisitedAt)
        default:
            return 0.0
            //return lastPoint.lastVistedAt
        }

    }

    public var polyline: MKPolyline {
        let coords = self.points.map({ (loc) -> CLLocationCoordinate2D in
            return loc.location!.coordinate
        })
        return MKPolyline(coordinates: coords, count: coords.count)
    }

}
