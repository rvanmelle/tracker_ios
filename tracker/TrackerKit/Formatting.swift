//
//  Formatting.swift
//  Tracker
//
//  Created by Reid van Melle on 2016-11-10.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

import Foundation
import CoreLocation

public extension Float {
    func format(_ num: String) -> String {
        return NSString(format: "%\(num)f" as NSString, self) as String
    }
}

public extension CLLocationDistance {
    func miles() -> String {
        let miles = Float(self)/1609.344
        return miles.format(".2")
    }

    func kms() -> String {
        let kms = Float(self) / 1000.0
        return kms.format(".2")
    }
}

public extension TimeInterval {
    func formatted(style: DateComponentsFormatter.UnitsStyle = .full) -> String {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = style
        formatter.allowedUnits = [NSCalendar.Unit.hour, NSCalendar.Unit.minute, NSCalendar.Unit.second]
        return formatter.string(from: self)!
    }
}
