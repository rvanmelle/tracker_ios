//
//  Date+Extensions.swift
//  Tracker
//
//  Created by Reid van Melle on 2016-10-23.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

import Foundation

extension Date {

    func date(byAddingYears years: Int) -> Date {
        let components = DateComponents(calendar: Calendar.current, year: years)
        let newDate = Calendar.current.date(byAdding: components, to: self)
        return newDate!
        //let dateComponents = gregorianCalendar.dateComponents([.day, .month, .year], from: self)
        //return gregorianCalendar.date(from: dateCompone nts)
    }

    public var beginningOfDay: Date {
        let cal = Calendar.current
        let components = cal.dateComponents(Set([.year, .month, .day]), from: self)
        return cal.date(from: components)!
    }

    /*
     - (NSDate *)beginningOfDay
     {
     NSCalendar *calendar = [NSCalendar currentCalendar];
     
     NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
     fromDate:self];
     
     return [calendar dateFromComponents:components];
     }

    */

}

/*
 
 #pragma mark - Adjusting Dates
 
 // Thaks, rsjohnson

 
 - (NSDate *) dateBySubtractingYears: (NSInteger) dYears
 {
 return [self dateByAddingYears:-dYears];
 }
 
 - (NSDate *) dateByAddingMonths: (NSInteger) dMonths
 {
 NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
 [dateComponents setMonth:dMonths];
 NSDate *newDate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:self options:0];
 return newDate;
 }
 
 - (NSDate *) dateBySubtractingMonths: (NSInteger) dMonths
 {
 return [self dateByAddingMonths:-dMonths];
 }
 
 // Courtesy of dedan who mentions issues with Daylight Savings
 - (NSDate *) dateByAddingDays: (NSInteger) dDays
 {
 NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
 [dateComponents setDay:dDays];
 NSDate *newDate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:self options:0];
 return newDate;
 }
 
 - (NSDate *) dateBySubtractingDays: (NSInteger) dDays
 {
 return [self dateByAddingDays: (dDays * -1)];
 }
 
 - (NSDate *) dateByAddingHours: (NSInteger) dHours
 {
 NSTimeInterval aTimeInterval = [self timeIntervalSinceReferenceDate] + D_HOUR * dHours;
 NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
 return newDate;
 }
 
 - (NSDate *) dateBySubtractingHours: (NSInteger) dHours
 {
 return [self dateByAddingHours: (dHours * -1)];
 }
 
 - (NSDate *) dateByAddingMinutes: (NSInteger) dMinutes
 {
 NSTimeInterval aTimeInterval = [self timeIntervalSinceReferenceDate] + D_MINUTE * dMinutes;
 NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
 return newDate;
 }
 
 - (NSDate *) dateBySubtractingMinutes: (NSInteger) dMinutes
 {
 return [self dateByAddingMinutes: (dMinutes * -1)];
 }
 
 - (NSDate *) dateByAddingSeconds: (NSInteger) dSeconds
 {
 NSTimeInterval aTimeInterval = [self timeIntervalSinceReferenceDate] + dSeconds;
 NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
 return newDate;
 
 }
 - (NSDate *) dateBySubtractingSeconds: (NSInteger) dSeconds
 {
 return [self dateByAddingSeconds: (dSeconds * -1)];
 }



 - (NSDate *)beginningOfDay
 {
 NSCalendar *calendar = [NSCalendar currentCalendar];
 
 NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
 fromDate:self];
 
 return [calendar dateFromComponents:components];
 }
 
 - (NSDate *)endOfDay
 {
 NSCalendar *calendar = [NSCalendar currentCalendar];
 
 NSDateComponents *components = [NSDateComponents new];
 components.day = 1;
 
 NSDate *date = [calendar dateByAddingComponents:components
 toDate:[self beginningOfDay]
 options:0];
 
 date = [date dateByAddingTimeInterval:-1];
 
 return date;
 }

*/
