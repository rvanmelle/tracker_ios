//
//  TableViewHelpers.swift
//  Reid van Melle
//
//  Created by Reid van Melle on 2017-02-13.
//  Copyright © 2017 Reid van Melle. All rights reserved.
//

import Foundation

/** Shortcuts for cell identifiers */
public extension UITableViewCell {
    public class func defaultIdentifier() -> String {
        return NSStringFromClass(self)
    }
}

public extension UITableViewHeaderFooterView {
    public class func defaultIdentifier() -> String {
        return NSStringFromClass(self)
    }
}

/** Register new cells */
public extension UITableView {
    public func register<T: UITableViewCell>(cellClass `class`: T.Type) {
        register(`class`, forCellReuseIdentifier: `class`.defaultIdentifier())
    }

    public func register<T: UITableViewCell>(nib: UINib, forClass `class`: T.Type) {
        register(nib, forCellReuseIdentifier: `class`.defaultIdentifier())
    }
}

/** Register new tables/headers */
public extension UITableView {
    public func register<T: UITableViewHeaderFooterView>(headerFooterClass `class`: T.Type) {
        register(`class`, forHeaderFooterViewReuseIdentifier: `class`.defaultIdentifier())
    }

    public func register<T: UITableViewHeaderFooterView>(nib: UINib, forHeaderFooterClass `class`: T.Type) {
        register(nib, forHeaderFooterViewReuseIdentifier: `class`.defaultIdentifier())
    }
}

/** Deques cells / headers */
public extension UITableView {
    public func dequeueReusableCell<T: UITableViewCell>(withClass `class`: T.Type) -> T? {
        return dequeueReusableCell(withIdentifier: `class`.defaultIdentifier()) as? T
    }

    public func dequeueReusableCell<T: UITableViewCell>(withClass `class`: T.Type,
                                                        forIndexPath indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: `class`.defaultIdentifier(), for: indexPath) as? T else {
            fatalError("Error: cell with identifier: \(`class`.defaultIdentifier()) for index path: "
                + "\(indexPath) is not \(T.self)")
        }
        return cell
    }

    public func dequeueResuableHeaderFooterView<T: UITableViewHeaderFooterView>(withClass `class`: T.Type) -> T? {
        return dequeueReusableHeaderFooterView(withIdentifier: `class`.defaultIdentifier()) as? T
    }
}
