//
//  TypedNotification.swift
//  Tracker
//
//  Created by Reid van Melle on 2017-04-01.
//  Copyright © 2017 Reid van Melle Inc. All rights reserved.
//

import Foundation

import Foundation

public protocol TypedNotification {
    var name: String { get }
    var content: TypedNotificationContentType? { get }
}

public protocol TypedNotificationContentType {
    init()
}

public class ObserverToken {
    fileprivate let observer: NSObjectProtocol

    fileprivate init(observer: NSObjectProtocol) {
        self.observer = observer
    }
}

public extension TypedNotification {
    static func post(_ notification: Self) {
        let name = Notification.Name(rawValue: notification.name)
        let userInfo = notification.content.map({ ["content": $0] })
        NotificationCenter.default.post(name: name, object: nil, userInfo: userInfo)
    }

    static func addObserver(_ notification: Self, observer: AnyObject, selector: Selector, object: AnyObject? = nil) {
        let name = Notification.Name(rawValue: notification.name)
        NotificationCenter.default.addObserver(observer, selector: selector, name: name, object: object)
    }

    static func removeObserver(_ notification: Self, observer: AnyObject, object: AnyObject? = nil) {
        let name = Notification.Name(rawValue: notification.name)
        NotificationCenter.default.removeObserver(observer, name: name, object: object)
    }

    static func addObserver(_ notification: Self, using block: @escaping () -> Void) -> ObserverToken {
        let name = Notification.Name(rawValue: notification.name)
        let observer = NotificationCenter.default.addObserver(forName: name, object: nil, queue: nil) { _ in
            block()
        }
        return ObserverToken(observer: observer)
    }

    static func addObserver
        <ContentType: TypedNotificationContentType>
        (_ notification: (ContentType) -> Self,
         using block: @escaping (ContentType) -> Void)
        -> ObserverToken {
            let name = Notification.Name(rawValue: notification(ContentType()).name)
            let observer = NotificationCenter.default.addObserver(forName: name, object: nil, queue: nil) { notification in
                if let content = notification.userInfo?["content"] as? ContentType {
                    block(content)
                }
            }
            return ObserverToken(observer: observer)
    }

    static func removeObserver(_ observer: ObserverToken) {
        NotificationCenter.default.removeObserver(observer.observer)
    }
}
