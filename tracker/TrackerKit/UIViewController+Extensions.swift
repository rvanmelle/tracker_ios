//
//  UIViewController+Extensions.swift
//  Tracker
//
//  Created by Reid van Melle on 2017-04-04.
//  Copyright © 2017 Reid van Melle Inc. All rights reserved.
//

import Foundation

public extension UIViewController {

    // MARK: - Alert and Dialog

    func simpleAlert(_ title: String?, message: String?, completion: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: completion)
    }

    func simpleCancelOKDialog(_ title: String?, message: String?, okHandler: ((UIAlertAction) -> Void)?,
                              cancelHandler: ((UIAlertAction) -> Void)? = nil) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: cancelHandler)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: okHandler)
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
        return alert
    }

    func confirmConfirmDestructiveDialog(_ title: String?, message: String?,
                                         confirmHandler: ((UIAlertAction) -> Void)?,
                                         cancelHandler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: cancelHandler)
        let confirmAction = UIAlertAction(title: "Confirm", style: .destructive, handler: confirmHandler)
        alert.addAction(cancelAction)
        alert.addAction(confirmAction)
        self.present(alert, animated: true, completion: nil)
    }

    // MARK: - Embedded Children

    func removeChildVC(_ childVC: UIViewController) {
        childVC.willMove(toParentViewController: nil)
        childVC.view.removeFromSuperview()
        childVC.removeFromParentViewController()
    }

    func embedFillingChildVC(_ childVC: UIViewController) {
        guard let containerView = self.view else { return }

        self.addChildViewController(childVC)
        containerView.addSubview(childVC.view)

        childVC.view.translatesAutoresizingMaskIntoConstraints = false
        containerView.pinItemFillHorizontally(childVC.view)
        containerView.pinItemFillVertically(childVC.view)
        //containerView.pinItem(childVC.view, attribute: .top, to: self.topLayoutGuide, toAttribute: .bottom)
        //containerView.pinItem(childVC.view, attribute: .bottom, to: self.bottomLayoutGuide, toAttribute: .top)

        childVC.didMove(toParentViewController: self)

        containerView.layoutIfNeeded()
    }
}
