//
//  TrackerKit.h
//  TrackerKit
//
//  Created by Reid van Melle on 2016-10-23.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for TrackerKit.
FOUNDATION_EXPORT double TrackerKitVersionNumber;

//! Project version string for TrackerKit.
FOUNDATION_EXPORT const unsigned char TrackerKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TrackerKit/PublicHeader.h>


