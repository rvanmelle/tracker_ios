//
//  DefaultDict.swift
//  Tracker
//
//  Created by Reid van Melle on 2016-11-10.
//  Copyright © 2016 Reid van Melle Inc. All rights reserved.
//

import Foundation

public protocol Initializable {
    init()
}

public extension Dictionary where Value: Initializable {
    // using key as external name to make it unambiguous from the standard subscript
    subscript(key key: Key) -> Value {
        mutating get { return self[key, or: Value()] }
        set { self[key] = newValue }
    }
}

// this can also be used in Swift 1.x
public extension Dictionary {
    subscript(key: Key, or def: Value) -> Value {
        mutating get {
            return self[key] ?? {
                // assign default value if self[key] is nil
                self[key] = def
                return def
                }()
        }
        set { self[key] = newValue }
    }
}
