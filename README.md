# Tracker #

This iOS application is an experimental location tracker similar to Moves. The idea is to experiment with various accuracy and power trade-offs available on the device such as:

* Significant location change
* Distance filters
* Deferred Updates

### What is this repository for? ###

* If you are interested in user location tracking on iOS devices.

### How do I get set up? ###

* Check out the project from github
* Run ./bootstrap in the tracker directory to build the carthage frameworks
* Run Tracker on your phone

### Contribution guidelines ###

Let me know if you have any ideas.